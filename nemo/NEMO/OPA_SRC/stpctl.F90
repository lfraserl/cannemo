MODULE stpctl
   !!======================================================================
   !!                       ***  MODULE  stpctl  ***
   !! Ocean run control :  gross check of the ocean time stepping
   !!======================================================================
   !! History :  OPA  ! 1991-03  (G. Madec) Original code
   !!            6.0  ! 1992-06  (M. Imbard)
   !!            8.0  ! 1997-06  (A.M. Treguier)
   !!   NEMO     1.0  ! 2002-06  (G. Madec)  F90: Free form and module
   !!            2.0  ! 2009-07  (G. Madec)  Add statistic for time-spliting
   !!----------------------------------------------------------------------

   !!----------------------------------------------------------------------
   !!   stp_ctl      : Control the run
   !!----------------------------------------------------------------------
   USE oce             ! ocean dynamics and tracers variables
   USE dom_oce         ! ocean space and time domain variables 
   USE sol_oce         ! ocean space and time domain variables 
   USE in_out_manager  ! I/O manager
   USE lbclnk          ! ocean lateral boundary conditions (or mpp link)
   USE lib_mpp         ! distributed memory computing
   USE dynspg_oce      ! pressure gradient schemes 
   USE c1d             ! 1D vertical configuration
   USE checksums, only : now_state_chksum

   IMPLICIT NONE
   PRIVATE

   PUBLIC stp_ctl           ! routine called by step.F90
   !!----------------------------------------------------------------------
   !! NEMO/OPA 3.3 , NEMO Consortium (2010)
   !! $Id: stpctl.F90 3294 2012-01-28 16:44:18Z rblod $
   !! Software governed by the CeCILL licence (NEMOGCM/NEMO_CeCILL.txt)
   !!----------------------------------------------------------------------

CONTAINS

   SUBROUTINE stp_ctl( kt, kindic )
      !!----------------------------------------------------------------------
      !!                    ***  ROUTINE stp_ctl  ***
      !!                     
      !! ** Purpose :   Control the run
      !!
      !! ** Method  : - Save the time step in numstp
      !!              - Print it each 50 time steps
      !!              - Print solver statistics in numsol 
      !!              - Stop the run IF problem for the solver ( indec < 0 )
      !!
      !! ** Actions :   'time.step' file containing the last ocean time-step
      !!                
      !!----------------------------------------------------------------------
      INTEGER, INTENT( in ) ::   kt         ! ocean time-step index
      INTEGER, INTENT( inout ) ::   kindic  ! indicator of solver convergence
      !!
      INTEGER  ::   ji, jj, jk              ! dummy loop indices
      INTEGER  ::   ii, ij, ik              ! temporary integers
      INTEGER  ::   numneg
      REAL(wp) ::   zmean
      REAL(wp) ::   zumax, zsmin, zssh2, zumax_local     ! temporary scalars
      REAL(wp) ::   ztmin, ztmax            ! temporary scalars
      INTEGER, DIMENSION(3) ::   ilocu      ! 
      INTEGER, DIMENSION(2) ::   ilocs      ! 
      !!----------------------------------------------------------------------

      IF( kt == nit000 .AND. lwp ) THEN
         WRITE(numout,*)
         WRITE(numout,*) 'stp_ctl : time-stepping control'
         WRITE(numout,*) '~~~~~~~'
         ! open time.step file
         CALL ctl_opn( numstp,  'time.step', 'REPLACE', 'FORMATTED', 'SEQUENTIAL', -1, numout, lwp, narea )
         CALL ctl_opn( numstat, 'time.stat', 'REPLACE', 'FORMATTED', 'SEQUENTIAL', -1, numout, lwp ) 
      ENDIF

      IF(lwp) WRITE ( numstp, '(1x, i8)' )   kt      !* save the current time step in numstp
      IF(lwp) REWIND( numstp )                       !  --------------------------

      !                                              !* Test maximum of velocity (zonal only)
      !                                              !  ------------------------
      !! zumax = MAXVAL( ABS( un(:,:,:) ) )                ! slower than the following loop on NEC SX5
      zumax = 0.e0
      zumax_local = 0.e0
      DO jk = 1, jpk
         DO jj = 1, jpj
            DO ji = 1, jpi
               zumax = MAX(zumax,ABS(un(ji,jj,jk)))
               if ( zumax > zumax_local ) then
                 zumax_local = zumax
                 ilocu(1) = ji
                 ilocu(2) = jj
                 ilocu(3) = jk
               endif
          END DO 
        END DO 
      END DO        
      IF( lk_mpp )   CALL mpp_max( zumax )                 ! max over the global domain
      !
      IF ( MOD( kt, nwrite ) == 1 .AND. lwp ) THEN
        WRITE(numout,*) ' ==>> time-step= ',kt,' abs(U) max: ', zumax
      ENDIF

      if ( .true. ) then
        !--- Find min/max in SST
        ztmin = HUGE(ztmin)
        ztmax = -HUGE(ztmax)
        DO jj = 2, jpjm1
           DO ji = 1, jpi
              IF( tmask(ji,jj,1) == 1) then
                ztmin = MIN(ztmin,tsn(ji,jj,1,jp_tem))
                ztmax = MAX(ztmax,tsn(ji,jj,1,jp_tem))
              ENDIF
           END DO
        END DO
        IF( lk_mpp )   CALL mpp_min( ztmin )   ! min over the global domain
        IF( lk_mpp )   CALL mpp_max( ztmax )   ! max over the global domain
        IF ( lwp ) THEN
          WRITE(numout,*) ' ==>> time-step= ',kt,' SST min,max: ', ztmin,ztmax
        ENDIF
      endif
      !
      IF( zumax > 20.e0 ) THEN
         IF( lk_mpp ) THEN
            CALL mpp_maxloc(ABS(un),umask,zumax,ii,ij,ik)
         ELSE
            ilocu = MAXLOC( ABS( un(:,:,:) ) )
            ii = ilocu(1) + nimpp - 1
            ij = ilocu(2) + njmpp - 1
            ik = ilocu(3)
         ENDIF
         IF(lwp) THEN
            WRITE(numout,cform_err)
            WRITE(numout,*) ' stpctl: the zonal velocity is larger than 20 m/s'
            WRITE(numout,*) ' ====== '
            WRITE(numout,9400) kt, zumax, ii, ij, ik
            WRITE(numout,*)
            WRITE(numout,*) '          output of last fields in numwso'
         ENDIF

         IF( lk_mpp .and. zumax_local > 20.e0 ) THEN
           !--- On each subdomain, write vel, T and S before, now and after (b,n,a)
           !--- at the grid point where the velocity is a max and > 20
           write(numout,*)'kt=',kt,'  nproc=',nproc,'  U_max=',zumax_local,'  b,n,a U: ', &
                        ub(ilocu(1),ilocu(2),ilocu(3)), &
                        un(ilocu(1),ilocu(2),ilocu(3)), &
                        ua(ilocu(1),ilocu(2),ilocu(3)), &
                        '  local i,j,k: ',ilocu
           write(numout,*)'kt=',kt,'  nproc=',nproc,'  U_max=',zumax_local,'  b,n,a V: ', &
                        vb(ilocu(1),ilocu(2),ilocu(3)), &
                        vn(ilocu(1),ilocu(2),ilocu(3)), &
                        va(ilocu(1),ilocu(2),ilocu(3)), &
                        '  local i,j,k: ',ilocu
           write(numout,*)'kt=',kt,'  nproc=',nproc,'  U_max=',zumax_local,'  W: ', &
                        wn(ilocu(1),ilocu(2),ilocu(3)), &
                        '  local i,j,k: ',ilocu
           write(numout,*)'kt=',kt,'  nproc=',nproc,'  U_max=',zumax_local,'  b,n,a Temp: ', &
                        tsb(ilocu(1),ilocu(2),ilocu(3),1), &
                        tsn(ilocu(1),ilocu(2),ilocu(3),1), &
                        tsa(ilocu(1),ilocu(2),ilocu(3),1), &
                        '  local i,j,k: ',ilocu
           write(numout,*)'kt=',kt,'  nproc=',nproc,'  U_max=',zumax_local,'  b,n,a Salt: ', &
                        tsb(ilocu(1),ilocu(2),ilocu(3),2), &
                        tsn(ilocu(1),ilocu(2),ilocu(3),2), &
                        tsa(ilocu(1),ilocu(2),ilocu(3),2), &
                        '  local i,j,k: ',ilocu
           call flush(numout)
         endif

         kindic = -3
      ENDIF
9400  FORMAT (' kt=',i6,' max abs(U): ',1pg11.4,', i j k: ',3i5)

      !                                              !* Test minimum of salinity
      !                                              !  ------------------------
      !! zsmin = MINVAL( tsn(:,:,1,jp_sal), mask = tmask(:,:,1) == 1.e0 )  slower than the following loop on NEC SX5
      zsmin = 100.e0
      DO jj = 2, jpjm1
         DO ji = 1, jpi
            IF( tmask(ji,jj,1) == 1) zsmin = MIN(zsmin,tsn(ji,jj,1,jp_sal))
         END DO
      END DO
      IF( lk_mpp )   CALL mpp_min( zsmin )                ! min over the global domain
      !
      IF( MOD( kt, nwrite ) == 1 .AND. lwp )   WRITE(numout,*) ' ==>> time-step= ',kt,' SSS min:', zsmin
      !
      IF( zsmin < 0.) THEN 
         IF (lk_mpp) THEN
            CALL mpp_minloc ( tsn(:,:,1,jp_sal),tmask(:,:,1), zsmin, ii,ij )
         ELSE
            ilocs = MINLOC( tsn(:,:,1,jp_sal), mask = tmask(:,:,1) == 1.e0 )
            ii = ilocs(1) + nimpp - 1
            ij = ilocs(2) + njmpp - 1
         ENDIF
         !
         IF(lwp) THEN
            WRITE(numout,cform_err)
            WRITE(numout,*) 'stp_ctl : NEGATIVE sea surface salinity'
            WRITE(numout,*) '======= '
            WRITE(numout,9500) kt, zsmin, ii, ij
            WRITE(numout,*)
            WRITE(numout,*) '          output of last fields in numwso'
         ENDIF
         kindic = -3
      ENDIF
9500  FORMAT (' kt=',i6,' min SSS: ',1pg11.4,', i j: ',2i5)
      IF (ln_chk_negsal) THEN
         zsmin = 0.
         zmean = 0.
         numneg = 0
         DO jk =1,jpk
            DO jj = 2, jpjm1
               DO ji = 1, jpi
                  IF (tsn(ji,jj,jk,jp_sal)*tmask(ji,jj,jk)<0.) THEN
                     zsmin = MIN(zsmin, tsn(ji,jj,jk,jp_sal))
                     zmean = zmean + tsn(ji,jj,jk,jp_sal)
                     numneg = numneg + 1
                 ENDIF
               END DO
            END DO
         END DO
         CALL mpp_min(zsmin)
         IF (zsmin < 0.) THEN
            CALL mpp_sum(zmean)
            CALL mpp_sum(numneg)
            IF (lwp) THEN
               WRITE(numout,*) "===Negative salinities detected"
               WRITE(numout,*) "   Global min      : ", zsmin
               WRITE(numout,*) "   Global mean     : ", zmean/numneg
               WRITE(numout,*) "   Number of points: ", numneg
               CALL FLUSH(numout)
            ENDIF
         ENDIF
      ENDIF
      ! Check if it's time for the now state global stats should be written
      IF( MOD(kt,nn_state_freq) == 0 ) THEN
         ! Write the final state of the model into a text file
         WRITE(numstat,'(A,X,I10.10)') 'Timestep: ', kt
         CALL now_state_chksum("  ", alt_unit = numstat) 
      ENDIF
      
      IF( lk_c1d )  RETURN          ! No log file in case of 1D vertical configuration

      ! log file (solver or ssh statistics)
      ! --------
      IF( lk_dynspg_flt ) THEN      ! elliptic solver statistics (if required)
         !
         IF(lwp) WRITE(numsol,9200) kt, niter, res, SQRT(epsr)/eps       ! Solver
         !
         IF( kindic < 0 .AND. zsmin > 0.e0 .AND. zumax <= 20.e0 ) THEN   ! create a abort file if problem found 
            IF(lwp) THEN
               WRITE(numout,*) ' stpctl: the elliptic solver DO not converge or explode'
               WRITE(numout,*) ' ====== '
               WRITE(numout,9200) kt, niter, res, sqrt(epsr)/eps
               WRITE(numout,*)
               WRITE(numout,*) ' stpctl: output of last fields'
               WRITE(numout,*) ' ======  '
            ENDIF
         ENDIF
         !
      ELSE                                   !* ssh statistics (and others...)
         IF( kt == nit000 .AND. lwp ) THEN   ! open ssh statistics file (put in solver.stat file)
            CALL ctl_opn( numsol, 'solver.stat', 'REPLACE', 'FORMATTED', 'SEQUENTIAL', -1, numout, lwp, narea )
         ENDIF
         !
         zssh2 = SUM( sshn(:,:) * sshn(:,:) * tmask_i(:,:) )
         IF( lk_mpp )   CALL mpp_sum( zssh2 )      ! sum over the global domain
         !
         IF(lwp) WRITE(numsol,9300) kt, zssh2, zumax, zsmin      ! ssh statistics
         !
      ENDIF

9200  FORMAT('it:', i8, ' iter:', i4, ' r: ',e16.10, ' b: ',e16.10 )
9300  FORMAT(' it :', i8, ' ssh2: ', e16.10, ' Umax: ',e16.10,' Smin: ',e16.10)
      !
   END SUBROUTINE stp_ctl

   !!======================================================================
END MODULE stpctl
